# Python-Based Course

This project a template project suitable for a Python-based course.

## Features

Provides a VS Code Dev Container configured with Python, pytest, useful
VS Code extensions, and file structure suitable for multiple assignments.
Some highlights include

* Each folder is an assignment.
* Each folder is also a Python package because it contains a `__init__.py` file.
    Be sure to create this file when creating a new assignment.
* This means the entire project is a single, unified, Python package hierarchy.
* This means that modules in one package (assignment) can import module from
    another.
* Likewise, you can drop a directory that contains libraries from a textbook
    that are also organized into packages, and then import them into assignment
    modules.
* Use relative imports to import modules in the same or sub-packages. This
    makes it easier to rename package names without breaking modules inside
    the package.
* Each assignment can have one or more entry point scripts (e.g., `main.py`).
    However, they must be ran as modules for their imports to work
    (e.g., `python -m hw1.main`). To simplify this, instructors can create a
    run profile in `.vscode/launch.json` for each entry point. They can
    then be launched from the `Run and Debug` panel on the left in VS Code.
    To run the an entrypoint in the debugger, set breakpoints, and then
    run the entrypoint as just described.
* This project includes PyTest. All tests can be ran from the `Testing`
    panel (beaker icon) in VS Code. To run tests in debug mode, set breakpoints,
    and run the test from the `Testing` panel using the play button with
    a bug on it.
* PlantUML is available from markdown files. For example

    ```plantuml
    @startuml
    a -> a
    @enduml
    ```

    Preview this document (`CTRL`+`SHIFT`+`v`) to see the above diagram
    rendered.
* There is also a PDF viewer installed.
* .gitignore and .gitattribute files configured for multiple operating systems.

## Prerequisites to install

* Git
* Docker Desktop
* VS Code
* Dev Container extension for VS Code

## Use

Clone this project, open it in VS Code, and `Reopen in Container`.

To push your work to your own repository, create an empty repository
with a service like GitLab or GitHub, then change your origin to point
to that repository and push

```bash
git remote set-url origin <paste-url-to-your-repo>
git push -u origin main
```
