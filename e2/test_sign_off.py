from .sign_off import sign_off

def test_sign_off_returns_im_out():
    assert sign_off() == "I'm out!"
