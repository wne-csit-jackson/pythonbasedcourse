from e1.greeter import greet
from .sign_off import sign_off

def main():
    print(greet())
    print(sign_off())

main()
