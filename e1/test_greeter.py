from .greeter import greet

def test_greet_returns_Hi():
    assert greet() == 'Hi'

